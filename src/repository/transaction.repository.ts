import { Injectable } from '@nestjs/common';
import { TransactionData } from '../entity/trasaction-data.entity';
import {
  And,
  Between,
  DataSource,
  Equal,
  LessThanOrEqual,
  MoreThanOrEqual,
  Repository,
} from 'typeorm';

@Injectable()
export class TransactionRepository extends Repository<TransactionData> {
  constructor(private readonly dataSource: DataSource) {
    super(TransactionData, dataSource.createEntityManager());
  }

  async saveTransaction(
    transaction: TransactionData,
  ): Promise<TransactionData> {
    return this.save(transaction);
  }

  async getTransactionByEmail(email: string): Promise<TransactionData[]> {
    return await this.find({
      where: { customer_email: email },
      order: { created_at: 'DESC' },
      take: 21,
    });
  }

  async getTransactionByDate(
    startDate: Date,
    lastDate: Date,
    email: string,
  ): Promise<TransactionData[]> {
    const findArgs = {
      where: {
        trx_date: Between(startDate.toISOString(), lastDate.toISOString()),
        customer_email: Equal(email),
      },
    };

    return (await this.findAndCount(findArgs)) as any;
  }
}
