import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Transport } from '@nestjs/microservices';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const microservice = app.connectMicroservice({
    transport: Transport.KAFKA,
    options: {
      client: {
        clientId: process.env.KAFKA_CLIENT_ID || 'service-loyalty',
        brokers: [process.env.KAFKA_BROKERS || 'localhost:19092'],
      },
      consumer: {
        groupId: process.env.KAFKA_GROUPID || 'service-loyalty',
      },
    },
  });

  await app.startAllMicroservices();
  await app.listen(3002);
}
bootstrap();
