import { Module } from '@nestjs/common';
import { LoyaltyModule } from './loyalty/loyalty.module';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { dataSourceOptions } from './config/data-source.config';
import { ClientsModule } from '@nestjs/microservices';
import { kafkaOptions } from './config/kafka.config';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot(dataSourceOptions),
    LoyaltyModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
