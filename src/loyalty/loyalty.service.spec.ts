import { Test, TestingModule } from '@nestjs/testing';
import { LoyaltyService } from './loyalty.service';
import { TransactionRepository } from '../repository/transaction.repository';
import { TransactionData } from '../entity/trasaction-data.entity';
import { CashbackLoyalty } from './cashback-loyalty';
import { Any } from 'typeorm';


describe('LoyaltyService', () => {
  let loyaltyService: LoyaltyService;
  let fakeTransactionRepo: Partial<TransactionRepository>;
  let cashbackLoyalty: CashbackLoyalty;

  beforeEach(async () => {
    fakeTransactionRepo = {
      saveTransaction: (transaction: TransactionData) =>
        Promise.resolve(new TransactionData()),
    };
    cashbackLoyalty = new CashbackLoyalty();

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        LoyaltyService,
        {
          provide: TransactionRepository,
          useValue: fakeTransactionRepo,
      },
        {
        provide: CashbackLoyalty,
          useValue: cashbackLoyalty,
      }],
    }).compile();

    loyaltyService = module.get<LoyaltyService>(LoyaltyService);
  });

  afterEach(() => jest.clearAllMocks());



  it('should be defined', () => {
    expect(loyaltyService).toBeDefined();
  });

  // it('should test', async() => {
  //   const email = 'jdoe@abc.com';

  //   jest
  //     .spyOn(loyaltyService, 'getTransactionByDate')
  //     .mockImplementationOnce(() => Promise.resolve(0));
  //   const mockedCurrentMonthTrans = {
  //     currentMonthTransactions: jest.fn(() => Promise.resolve(0)),
  //   }

  //   const mockedLastMonthTrans = {
  //     lastMonthTransactions: jest.fn(() => Promise.resolve(0)),
  //   }

  //   const mocked2MonthTrans = {
  //     transactions2MonthsBefore: jest.fn(() => Promise.resolve(0)),
  //   }

  //   const mocked3MonthTrans = {
  //     transactions3MonthsBefore: jest.fn(() => Promise.resolve(0)),
  //   }


  //   const result = loyaltyService.calculateCashback(
  //     'new Date()',
  //     'test@email.com',
  //   );

  //   expect(result).toBe(0)
  // });
});
