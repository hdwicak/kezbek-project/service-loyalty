import { Injectable } from '@nestjs/common';
// import { TransactionDataDto } from 'src/dto/transaction-data.dto';
// import { TransactionData } from 'src/entity/trasaction-data.entity';
// import { TransactionRepository } from 'src/repository/transaction.repository';
import { CashbackLoyalty } from './cashback-loyalty';
import { TransactionRepository } from '../repository/transaction.repository';
import { TransactionDataDto } from '../dto/transaction-data.dto';
import { TransactionData } from '../entity/trasaction-data.entity';

@Injectable()
export class LoyaltyService {
  constructor(
    private readonly transactionRepository: TransactionRepository,
    private readonly cashBackLoyalty: CashbackLoyalty,
  ) {}

  async save(transaction: TransactionDataDto): Promise<TransactionData> {
    return await this.transactionRepository.save(transaction);
  }

  async getTransactionByDate(
    startDate: Date,
    lastDate: Date,
    email: string,
  ): Promise<number> {
    const countTranasaction = this.transactionRepository
      .createQueryBuilder('transactions')
      .where('transactions.trx_date >= :startDate', {
        startDate: startDate,
      })
      .andWhere('transactions.trx_date <= :lastDate', {
        lastDate: lastDate,
      })
      .andWhere('transactions.customer_email = :email', { email: email })
      .getCount();

    return countTranasaction;
  }

  async calculateCashback(trxDate: string, email: string) {
    const trxDateObject = new Date(trxDate);

    const firstDateThisMonth = new Date(
      trxDateObject.getFullYear(),
      trxDateObject.getMonth(),
      1,
    );

    const lastDateThisMonth = new Date(
      trxDateObject.getFullYear(),
      trxDateObject.getMonth() + 1,
      0,
    );

    const firstDateLastMonth = new Date(
      trxDateObject.getFullYear(),
      trxDateObject.getMonth() - 1,
      1,
    );

    const lastDateLastMonth = new Date(
      trxDateObject.getFullYear(),
      trxDateObject.getMonth(),
      0,
    );

    const firstDate2MonthsBefore = new Date(
      trxDateObject.getFullYear(),
      trxDateObject.getMonth() - 2,
      1,
    );

    const lastDate2MonthsBefore = new Date(
      trxDateObject.getFullYear(),
      trxDateObject.getMonth() - 1,
      0,
    );

    const firstDate3MonthsBefore = new Date(
      trxDateObject.getFullYear(),
      trxDateObject.getMonth() - 3,
      1,
    );
    const lastDate3MonthsBefore = new Date(
      trxDateObject.getFullYear(),
      trxDateObject.getMonth() - 2,
      0,
    );

    const currentMonthTransactions = await this.getTransactionByDate(
      firstDateThisMonth,
      lastDateThisMonth,
      email,
    );

    const lastMonthTransactions = await this.getTransactionByDate(
      firstDateLastMonth,
      lastDateLastMonth,
      email,
    );

    const transactions2MonthsBefore = await this.getTransactionByDate(
      firstDate2MonthsBefore,
      lastDate2MonthsBefore,
      email,
    );

    const transactions3MonthsBefore = await this.getTransactionByDate(
      firstDate3MonthsBefore,
      lastDate3MonthsBefore,
      email,
    );

    const loyaltyCashback = this.cashBackLoyalty.calculateCashback(
      currentMonthTransactions,
      lastMonthTransactions,
      transactions2MonthsBefore,
      transactions3MonthsBefore,
    );

    return loyaltyCashback;
  }

  async fallbackTransaction(orderId, customerEmail){
    const foundData = await this.transactionRepository.findOneBy({
      order_id: orderId,
      customer_email: customerEmail,
    });
    return await this.transactionRepository.remove(foundData);
  }
}
