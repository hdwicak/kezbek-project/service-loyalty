import { Body, Controller, Inject, Logger, Post } from '@nestjs/common';
import { LoyaltyService } from './loyalty.service';
import * as dotenv from 'dotenv';
import { ClientKafka, EventPattern, Payload } from '@nestjs/microservices';
import { LoyaltyMessageDto } from 'src/dto/loyalty_message.dto';
import { TransactionData } from 'src/entity/trasaction-data.entity';
import { TransactionDataDto } from 'src/dto/transaction-data.dto';

dotenv.config();

@Controller('loyalty')
export class LoyaltyController {
  constructor(
    private readonly loyaltyService: LoyaltyService,

    @Inject(process.env.KAFKA_TOKEN || 'KAFKA_SERVICE_LOYALTY')
    private readonly client: ClientKafka,

    private readonly logger: Logger,
  ) {}

  @EventPattern('incoming-service-loyalty')
  async processMessage(@Payload() data: LoyaltyMessageDto) {
    try {
      this.logger.log(
        '[KEZBEK-SERVICE-LOYALTY] processing incoming message topic : incoming-service-loyalty',
      );
      const email = data['customer_email'];
      const trxDate = data['trx_date'];
      const loyaltyCashback = await this.loyaltyService.calculateCashback(
        trxDate,
        email,
      );

      const addEntryMessage = {
        cashback_loyalty: loyaltyCashback,
        cashback_total: data['cashback_promo'] + loyaltyCashback,
      };
      const messageLoyalty = { ...data, ...addEntryMessage };

      //save transactions to dB
      await this.loyaltyService.save(messageLoyalty);
      await this.client.emit(
        'incoming-service-transaction',
        JSON.stringify(messageLoyalty),
      );
      this.logger.log(
        '[KEZBEK-SERVICE-LOYALTY] Done processing cashback loyalty; emit message to topic : incoming-service-transaction  ',
      );
    } catch (err) {
      this.logger.log(
        '[KEZBEK-SERVICE-LOYALTY] error processing cashback loyalty, emit message to topic : error-transaction',
      );
      this.client.emit('error-transaction', JSON.stringify(data));
      this.logger.error(err);
    }
  }

  @EventPattern('error-payment')
  async fallbackTransaction(@Payload() data: TransactionDataDto) {
    try {
      this.logger.log('[KEZBEK-SERVICE-LOYALTY] processing message from topic : error-payment');
      const orderId = data.order_id;
      const customerEmail = data.customer_email;

      await this.loyaltyService.fallbackTransaction(orderId, customerEmail);
      await this.client.emit('error-transaction', JSON.stringify(data));
      this.logger.log(
        '[KEZBEK-SERVICE-LOYALTY] fallback transaction and emit message to topic : error-transaction',
      );
    } catch (err) {
      this.logger.error(err);
    }
  }

  @Post()
  async save(@Body() transaction: TransactionDataDto) {
    return await this.loyaltyService.save(transaction);
  }

  // @Post('transactions')
  // async getCashback(@Body() transaction: TransactionDataDto) {
  //   const email = transaction['customer_email'];
  //   const trxDate = transaction['trx_date'];

  //   return await this.loyaltyService.calculateCashback(trxDate, email);
  // }
}
