import { Logger, Module } from '@nestjs/common';
import { LoyaltyController } from './loyalty.controller';
import { LoyaltyService } from './loyalty.service';
import { TransactionData } from 'src/entity/trasaction-data.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientsModule } from '@nestjs/microservices';
import { TransactionRepository } from 'src/repository/transaction.repository';
import { kafkaOptions } from 'src/config/kafka.config';
import { CashbackLoyalty } from './cashback-loyalty';

@Module({
  imports: [
    TypeOrmModule.forFeature([TransactionData]),
    ClientsModule.register(kafkaOptions),
  ],
  controllers: [LoyaltyController],
  providers: [LoyaltyService, TransactionRepository, CashbackLoyalty, Logger],
})
export class LoyaltyModule {}
