/* eslint-disable @typescript-eslint/no-namespace */
export const loyaltyRule = {
  basic_tier: {
    name_tier: 'BASIC',
    class: 1,
    min_transactions: 1,
    max_transactions: 5,
    rule: [],
  },
  bronze_tier: {
    name_tier: 'BRONZE',
    class: 2,
    min_transactions: 6,
    max_transactions: 12,
    rule: [
      [8, 15000],
      [10, 25000],
      [12, 35000],
    ],
  },
  silver_tier: {
    name_tier: 'SILVER',
    class: 3,
    min_transactions: 13,
    max_transactions: 19,
    rule: [
      [15, 13500],
      [17, 23500],
      [19, 33500],
    ],
  },
  gold_tier: {
    name_tier: 'GOLD',
    class: 4,
    min_transactions: 20,
    max_transactions: 100,
    rule: [
      [22, 13500],
      [24, 23500],
      [26, 33500],
      [27, 33500],
      [28, 33500],
      [27, 33500],
      [28, 33500],
      [29, 33500],
      [30, 33500],
      [31, 33500],
      [32, 33500],
    ],
  },
};

export namespace TIER {
  export const BASIC = 'BASIC';
  export const BRONZE = 'BRONZE';
  export const SILVER = 'SILVER';
  export const GOLD = 'GOLD';
}
