import { Injectable } from '@nestjs/common';
import { TIER, loyaltyRule } from './ruleLoyalty';

@Injectable()
export class CashbackLoyalty {
  async calculateCashback(
    currentMonthTrans: number,
    lastMonthTrans: number,
    trans2MonthBefore: number,
    trans3MonthBefore: number,
  ) {
    let finalCashbackAmount = 0;

    const rule = Object.values(loyaltyRule);

    const filteredTier = (count, rule) => {
      let tier;
      let outputTier;
      if (count != 0) {
        rule.map((item) => {
          if (
            item.min_transactions <= count &&
            count <= item.max_transactions
          ) {
            tier = item.name_tier;
          }
        });
        outputTier = rule.filter((item) => item.name_tier === tier);
      } else {
        outputTier = rule.filter((item) => item.class === 1);
      }

      return outputTier;
    };

    const getBaseCount = (dbLastMonth, db2MonthsBefore, db3MonthsBefore) => {
      let baseCountTransaction;

      if (dbLastMonth > 0) {
        const tierLastMonth = filteredTier(dbLastMonth, rule);

        baseCountTransaction = tierLastMonth[0].min_transactions;

        return baseCountTransaction;
      }

      if (dbLastMonth === 0 && db2MonthsBefore > 0) {
        const tier2MonthsBefore = filteredTier(db2MonthsBefore, rule);

        if (tier2MonthsBefore[0].class > 1) {
          //lower tier 1 level
          const updatedTier = rule.filter(
            (item) => item.class === tier2MonthsBefore[0].class - 1,
          );
          baseCountTransaction = updatedTier[0].min_transactions;
          return baseCountTransaction;
        } else {
          baseCountTransaction = rule.filter((item) => item.class === 1)[0]
            .min_transactions;
          return baseCountTransaction;
        }
      }

      if (dbLastMonth === 0 && db2MonthsBefore === 0 && db3MonthsBefore === 0) {
        //set to basic class
        baseCountTransaction = rule.filter((item) => item.class === 1)[0]
          .min_transactions;
        return baseCountTransaction;
      }

      if (dbLastMonth === 0 && db2MonthsBefore === 0) {
        const tier3MonthsBefore = filteredTier(db3MonthsBefore, rule);
        //lower tier 2 level
        console.log(tier3MonthsBefore);
        if (tier3MonthsBefore[0].class > 2) {
          const updatedTier = rule.filter(
            (item) => item.class === tier3MonthsBefore[0].class - 2,
          );
          baseCountTransaction = updatedTier[0].min_transactions;
          return baseCountTransaction;
        } else {
          baseCountTransaction = rule.filter((item) => item.class === 1)[0]
            .min_transactions;
          return baseCountTransaction;
        }
      }
    };

    const prevMonthBaseCount = getBaseCount(
      // currentMonthTrans,
      lastMonthTrans,
      trans2MonthBefore,
      trans3MonthBefore,
    );

    const prevMonthRule = rule.filter(
      (item) => item.min_transactions === prevMonthBaseCount,
    );
    // console.log(`previous month base count = ${prevMonthBaseCount}`);
    // console.log(`current month transction = ${currentMonthTrans}`);
    // console.log('rule base on prevMonth')
    // console.log(prevMonthRule);

    const lastMonthRule = rule.filter(
      (item) => item.min_transactions === prevMonthBaseCount,
    );

    const filteredTierThisMonth = filteredTier(currentMonthTrans, rule);
    const currentMonthRule = rule.filter(
      (item) =>
        item.min_transactions === filteredTierThisMonth[0].min_transactions,
    );

    const currentMonthBaseCount = currentMonthRule[0].min_transactions;

    // console.log('rule base on current Month')
    // console.log( currentMonthRule);
    // console.log('current month base count = ', currentMonthBaseCount)

    let cekCashback = 0;

    if (prevMonthBaseCount === 1) {
      const ruleApplied = currentMonthRule[0].rule;
      if (ruleApplied.length > 0 && prevMonthBaseCount === 1) {
        ruleApplied.forEach((item) => {
          if (currentMonthTrans + 1 === item[0]) {
            cekCashback = item[1];
          }
        });
        finalCashbackAmount = cekCashback;
      }
    } else {
      const ruleApplied = prevMonthRule[0].rule;
      if (ruleApplied.length > 0 && prevMonthBaseCount > 1) {
        ruleApplied.forEach((item) => {
          if (prevMonthBaseCount + currentMonthTrans + 1 === item[0]) {
            cekCashback = item[1];
          }
        });
        finalCashbackAmount = cekCashback;
      }
    }
    return finalCashbackAmount;
  }
}
