import { CashbackLoyalty } from './cashback-loyalty';

const cashbackLoyalty = new CashbackLoyalty();

describe('loyalty cashback', () => {
  it('should return 0 cashback loyalty', async () => {
    const currentMonthTrans = 0;
    const lastMonthTrans = 0;
    const trans2MonthBefore = 0;
    const trans3MonthBefore = 0;

    const result = await cashbackLoyalty.calculateCashback(
      currentMonthTrans,
      lastMonthTrans,
      trans2MonthBefore,
      trans3MonthBefore,
    );

    expect(result).toBe(0);
  });

  it('should return 0 cashback loyalty', async () => {
    const currentMonthTrans = 4;
    const lastMonthTrans = 0;
    const trans2MonthBefore = 0;
    const trans3MonthBefore = 0;

    const result = await cashbackLoyalty.calculateCashback(
      currentMonthTrans,
      lastMonthTrans,
      trans2MonthBefore,
      trans3MonthBefore,
    );
    expect(result).toBe(0);
  });

  it('should return 15000 cashback loyalty for Bronze Tier', async () => {
    const currentMonthTrans = 7;
    const lastMonthTrans = 0;
    const trans2MonthBefore = 0;
    const trans3MonthBefore = 0;

    const result = await cashbackLoyalty.calculateCashback(
      currentMonthTrans,
      lastMonthTrans,
      trans2MonthBefore,
      trans3MonthBefore,
    );
    expect(result).toBe(15000);
  });

  it('should return 25000 cashback loyalty for Bronze Tier', async () => {
    const currentMonthTrans = 9;
    const lastMonthTrans = 0;
    const trans2MonthBefore = 0;
    const trans3MonthBefore = 0;

    const result = await cashbackLoyalty.calculateCashback(
      currentMonthTrans,
      lastMonthTrans,
      trans2MonthBefore,
      trans3MonthBefore,
    );
    expect(result).toBe(25000);
  });

  it('should return 35000 cashback loyalty for Bronze Tier', async () => {
    const currentMonthTrans = 11;
    const lastMonthTrans = 0;
    const trans2MonthBefore = 0;
    const trans3MonthBefore = 0;

    const result = await cashbackLoyalty.calculateCashback(
      currentMonthTrans,
      lastMonthTrans,
      trans2MonthBefore,
      trans3MonthBefore,
    );
    expect(result).toBe(35000);
  });

  it('should return 0 cashback loyalty for Silver Tier', async () => {
    const currentMonthTrans = 13;
    const lastMonthTrans = 0;
    const trans2MonthBefore = 0;
    const trans3MonthBefore = 0;

    const result = await cashbackLoyalty.calculateCashback(
      currentMonthTrans,
      lastMonthTrans,
      trans2MonthBefore,
      trans3MonthBefore,
    );
    expect(result).toBe(0);
  });

  it('should return 13500 cashback loyalty for Silver Tier', async () => {
    const currentMonthTrans = 14;
    const lastMonthTrans = 0;
    const trans2MonthBefore = 0;
    const trans3MonthBefore = 0;

    const result = await cashbackLoyalty.calculateCashback(
      currentMonthTrans,
      lastMonthTrans,
      trans2MonthBefore,
      trans3MonthBefore,
    );
    expect(result).toBe(13500);
  });

  it('should return 23500 cashback loyalty for Silver Tier', async () => {
    const currentMonthTrans = 16;
    const lastMonthTrans = 0;
    const trans2MonthBefore = 0;
    const trans3MonthBefore = 0;

    const result = await cashbackLoyalty.calculateCashback(
      currentMonthTrans,
      lastMonthTrans,
      trans2MonthBefore,
      trans3MonthBefore,
    );
    expect(result).toBe(23500);
  });

  it('should return 33500 cashback loyalty for Silver Tier', async () => {
    const currentMonthTrans = 18;
    const lastMonthTrans = 0;
    const trans2MonthBefore = 0;
    const trans3MonthBefore = 0;

    const result = await cashbackLoyalty.calculateCashback(
      currentMonthTrans,
      lastMonthTrans,
      trans2MonthBefore,
      trans3MonthBefore,
    );
    expect(result).toBe(33500);
  });

  it('should return 0 cashback loyalty for Gold Tier', async () => {
    const currentMonthTrans = 19;
    const lastMonthTrans = 0;
    const trans2MonthBefore = 0;
    const trans3MonthBefore = 0;

    const result = await cashbackLoyalty.calculateCashback(
      currentMonthTrans,
      lastMonthTrans,
      trans2MonthBefore,
      trans3MonthBefore,
    );
    expect(result).toBe(0);
  });

  it('should return 13500 cashback loyalty for Gold Tier', async () => {
    const currentMonthTrans = 21;
    const lastMonthTrans = 0;
    const trans2MonthBefore = 0;
    const trans3MonthBefore = 0;

    const result = await cashbackLoyalty.calculateCashback(
      currentMonthTrans,
      lastMonthTrans,
      trans2MonthBefore,
      trans3MonthBefore,
    );
    expect(result).toBe(13500);
  });

  it('should return 23500 cashback loyalty for Gold Tier', async () => {
    const currentMonthTrans = 23;
    const lastMonthTrans = 0;
    const trans2MonthBefore = 0;
    const trans3MonthBefore = 0;

    const result = await cashbackLoyalty.calculateCashback(
      currentMonthTrans,
      lastMonthTrans,
      trans2MonthBefore,
      trans3MonthBefore,
    );
    expect(result).toBe(23500);
  });

  it('should return 33500 cashback loyalty for Gold Tier', async () => {
    const currentMonthTrans = 25;
    const lastMonthTrans = 0;
    const trans2MonthBefore = 0;
    const trans3MonthBefore = 0;

    const result = await cashbackLoyalty.calculateCashback(
      currentMonthTrans,
      lastMonthTrans,
      trans2MonthBefore,
      trans3MonthBefore,
    );
    expect(result).toBe(33500);
  });

  it('should return 0 cashback loyalty for Gold Tier after 7x max cashback', async () => {
    const currentMonthTrans = 35;
    const lastMonthTrans = 0;
    const trans2MonthBefore = 0;
    const trans3MonthBefore = 0;

    const result = await cashbackLoyalty.calculateCashback(
      currentMonthTrans,
      lastMonthTrans,
      trans2MonthBefore,
      trans3MonthBefore,
    );
    expect(result).toBe(0);
  });

  it('should return 23500 cashback loyalty for Gold Tier after 4 transaction this month', async () => {
    const currentMonthTrans = 3;
    const lastMonthTrans = 25;
    const trans2MonthBefore = 0;
    const trans3MonthBefore = 0;

    const result = await cashbackLoyalty.calculateCashback(
      currentMonthTrans,
      lastMonthTrans,
      trans2MonthBefore,
      trans3MonthBefore,
    );
    expect(result).toBe(23500);
  });

  it('should return 33500 cashback loyalty for Gold Tier after 9 transaction this month', async () => {
    const currentMonthTrans = 8;
    const lastMonthTrans = 25;
    const trans2MonthBefore = 0;
    const trans3MonthBefore = 0;

    const result = await cashbackLoyalty.calculateCashback(
      currentMonthTrans,
      lastMonthTrans,
      trans2MonthBefore,
      trans3MonthBefore,
    );
    expect(result).toBe(33500);
  });

  it('should return 23500 cashback loyalty, last-month = 0 ; 2-month-before :27 ; this month = 3', async () => {
    const currentMonthTrans = 3;
    const lastMonthTrans = 0;
    const trans2MonthBefore = 27;
    const trans3MonthBefore = 0;

    const result = await cashbackLoyalty.calculateCashback(
      currentMonthTrans,
      lastMonthTrans,
      trans2MonthBefore,
      trans3MonthBefore,
    );
    expect(result).toBe(23500);
  });
  it('should return 13500 cashback loyalty, last-month = 0 ; 2-month-before :27 ; this month = 1', async () => {
    const currentMonthTrans = 1;
    const lastMonthTrans = 0;
    const trans2MonthBefore = 27;
    const trans3MonthBefore = 0;

    const result = await cashbackLoyalty.calculateCashback(
      currentMonthTrans,
      lastMonthTrans,
      trans2MonthBefore,
      trans3MonthBefore,
    );
    expect(result).toBe(13500);
  });

  it('should return 33500 cashback loyalty, last-month = 0 ; 2-month-before :27 ; this month = 1', async () => {
    const currentMonthTrans = 5;
    const lastMonthTrans = 0;
    const trans2MonthBefore = 27;
    const trans3MonthBefore = 0;

    const result = await cashbackLoyalty.calculateCashback(
      currentMonthTrans,
      lastMonthTrans,
      trans2MonthBefore,
      trans3MonthBefore,
    );
    expect(result).toBe(33500);
  });

  it('should return 0 cashback loyalty, last-month = 0 ; 2-month-before :27 ; this month = 20', async () => {
    const currentMonthTrans = 20;
    const lastMonthTrans = 0;
    const trans2MonthBefore = 27;
    const trans3MonthBefore = 0;

    const result = await cashbackLoyalty.calculateCashback(
      currentMonthTrans,
      lastMonthTrans,
      trans2MonthBefore,
      trans3MonthBefore,
    );
    expect(result).toBe(0);
  });

});
